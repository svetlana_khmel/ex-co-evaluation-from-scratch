
### `yarn run dev`

Runs the app in the development mode with webpackDevServer.<br />
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `yarn run eslint`

Runs linter to validate the code.<br />

### `yarn run eslint:fix`

Runs linter to validate the code  and fixes some issues.<br />

This repository is for practicing this course:

(Aws amplify)

https://www.linkedin.com/learning/aws-and-react-creating-full-stack-apps/full-stack-react-development-on-aws
https://aws.amazon.com/getting-started/tutorials/

- Start with amplify:
https://aws.amazon.com/getting-started/tutorials/deploy-react-app-cicd-amplify


Go to 
https://aws.amazon.com/amplify
To get started

== AWS S3 hosting with Amplify:

AWS Amplify is a development platform for building secure, scalable mobile and web applications. It makes it easy for you to authenticate users, securely store data and user metadata, authorize selective access to data, integrate machine learning, analyze application metrics, and execute server-side code. Amplify covers the complete mobile application development workflow from version control, code testing, to production deployment, and it easily scales with your business from thousands of users to tens of millions. The Amplify libraries and CLI, part of the Amplify Framework, are open source and offer a pluggable interface that enables you to customize and create your own plugins.

1. Create account
2. Install CLI:
npm install -g @aws-amplify/cli
amplify configure (should the latest version of node)

-region (any, was-2),
-default name
-programmatic access

Here a lot of info and video :
https://us-east-2.console.aws.amazon.com/console/home?region=us-east-2

Follow console’s direction:
1. Create user
2. Key:
AKIA5AWYJZWDVQUU4GXT

1. COPY! Secret key
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

==Hosting s React App on AWS
Init project:
> amplify init
Default editor:Visual Studio

For more information on AWS Profiles, see:
https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html

> amplify hosting add 
OR 
amplify (to see all available commands)

Account ID or alias 894880566663

Troubleshooting:
OptInRequired: The AWS Access Key Id needs a subscription for the service
This error message indicates that you have not signed up for Amazon Simple Email Service. Even if you currently have an AWS account - with an Access Key Id and a Secret Key - you still must explicitly sign up for Amazon SES.

Please double-check that you've signed up. Here's one way to tell:
* Go to http://aws.amazon.com. Click the Account tab, and then Usage Reports. 
* Sign in with your Amazon user ID and password. 
* On the Usage Reports page, look for a drop-down list of services that you have subscribed to. Verify that Amazon SES is in that list. 

If you don't see Amazon SES there, please go to http://aws.amazon.com/ses and click the button marked "Sign up for Amazon SES".

Troubleshooting:
OptInRequired: The AWS Access Key Id needs a subscription for the service

I had the same issue. After I verified my mobile phone number using their automatic call process, this error message went away.


https://console.aws.amazon.com/support/home#/case/?displayId=6835860931&language=en

https://portal.aws.amazon.com/billing/signup?type=resubscribe&src=header-abnr&redirect_url=https%3A%2F%2Faws.amazon.com%2Fregistration-confirmation#/identityverification




version: 0.1
frontend:
  phases:
    preBuild:
      commands:
        - yarn install
    build:
      commands:
        - node ./node_modules/webpack/bin/webpack.js
  artifacts:
    baseDirectory: dist
    files:
      - '**/*'
  cache:
    paths:
      - node_modules/**/*
      
      
      
      
      I have checked your account and I can confirm that you have completed the phone number verification and add a valid payment method to your account.
      
      All that is left for you to do is to subscribe to AWS services by clicking the link below and selecting Basic support: 
      
      https://aws-portal.amazon.com/gp/aws/developer/registration/index.html 
      
      If you have created any IAM users, please make sure you are logging in as the root user when signing up for services. Please note that when you sign up for a new AWS account, the process of validating your credit card information and activating your services can take up to 24 hours.
      
      To get started with AWS, please visit our Getting Started guide:
      
      https://aws.amazon.com/documentation/gettingstarted/ 
      
      I hope this helps. 
      
      Best regards,
      
      Brad N
      Amazon Web Services
      
      Check out the AWS Support Knowledge Center, a knowledge base of articles and videos that answer customer questions about AWS services: https://aws.amazon.com/premiumsupport/knowledge-center/?icmpid=support_email_category 

