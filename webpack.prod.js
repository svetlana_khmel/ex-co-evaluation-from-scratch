const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const MinifyPlugin = require('babel-minify-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = merge(common, {
   mode: 'production',
   module: {
    rules: []
   },
   plugins: [
    new MinifyPlugin(),
    new MiniCssExtractPlugin()
   ]
});