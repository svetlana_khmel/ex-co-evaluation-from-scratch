import React, { Component } from 'react';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import {
    Link,
} from 'react-router-dom';

import './itemPage.css';

class ItemPage extends Component {
    render() {
        let { itemData } = this.props.location.state;

        itemData
            ? localStorage.setItem('currentVideo', JSON.stringify(itemData))
            : localStorage.getItem('currentVideo');

        itemData = JSON.parse(itemData);

        return (
            <>
                <Link className="back" to="/">&larr;BACK</Link>
                <div className="display-item item-block">
                    {itemData.url
                        ? this.videoSourceConfig.getPlayer(itemData[this.videoSourceConfig.id])
                        : (itemData.thumbnail
                            ? <img alt="" src={itemData.thumbnail} className="videoThumbnail" />
                            : <div className="videoThumbnail" />)}
                    <div className="display-item__details item">
                        <h2>{itemData.title}</h2>
                        <div className="display-item__bottom-bar">
                            <div className="ex-icon" />
                        </div>
                    </div>
                    <div className="social">
                        <div className="facebook-icon" />
                        <div className="pinterest-icon" />
                        <div className="twitter-icon" />
                        <div className="swarm-icon" />
                        <div className="tumblr-icon" />
                    </div>
                </div>
            </>
        );
    }
}

ItemPage.propTypes = {
    location: PropTypes.shape({
        pathname: PropTypes.string,
        state: PropTypes.shape({
            itemData: PropTypes.string
        }),
        search: PropTypes.string,
        hash: PropTypes.string,
        key: PropTypes.string,
    }),
};

export default withRouter(ItemPage);
