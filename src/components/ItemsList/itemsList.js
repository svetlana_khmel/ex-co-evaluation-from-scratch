import React, { Component } from 'react';
import {
    Switch,
    Route,
    Link,
} from 'react-router-dom';
import PropTypes from 'prop-types';

import './ItemsList.css';
import '../DisplayItem/DisplayItem.css';

import DisplayItem from '../DisplayItem/DispalyItem';
import { VideoSourceTypes } from '../../types/VideoSourceTypes';
import ItemPage from '../../pages/itemPage';

class ItemsList extends Component {
  supportedItems = (items) => {
      const approved = items.filter(item => !!(VideoSourceTypes.find(source => source.name === item.source)));
      return approved;
  };

  render() {
      const items = this.supportedItems(this.props.items).map((item, index) => (
          <Link
              key={item.videoId ? item.videoId : index}
              className="display-item"
              to={{
                  pathname: `/item/:${item.itemId}`,
                  state: {
                      itemData: `${JSON.stringify(item)}`,
                  },
              }}
          >
              <DisplayItem itemData={item} />
          </Link>
      ));

      localStorage.removeItem('currentVideo');

      return (
          <div className="items-list">
              <Switch>
                  <Route exact path="/">
                      <div className="item-list__header">
                          <h1>Most viewed</h1>
                      </div>
                      <div className="items-list__content">{items}</div>
                  </Route>
                  <Route path="/:id"><ItemPage /></Route>
              </Switch>
          </div>
      );
  }
}

ItemsList.propTypes = {
    items: PropTypes.array,
};

export default ItemsList;

