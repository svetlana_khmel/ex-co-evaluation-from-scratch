import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './DisplayItem.css';

import { VideoSourceTypes } from '../../types/VideoSourceTypes';

class displayItem extends Component {
  videoSourceConfig = VideoSourceTypes.find((source) => {
      const { itemData } = this.props;

      return itemData.source && source.name === itemData.source;
  });

  isValidItem = (type, source) => !!(type && source);

  numberToReadableText(num) {
      const number = Math.abs(Number(num));
      return number >= 1.0e+9
          ? `${number / 1.0e+9}B` : number >= 1.0e+6
              ? `${Math.round((number / 1.0e+6) * 10) / 10}M` : number >= 1.0e+3
                  ? `${number / 1.0e+3}K` : number;
  }

  render() {
      const { itemData } = this.props;

      if (this.isValidItem(itemData.type, itemData.source)) {
          const readableViewsNumber = this.numberToReadableText(itemData.views);

          return (
              <div>
                  {itemData.url
                      ? this.videoSourceConfig.getPlayer(itemData[this.videoSourceConfig.id])
                      : (itemData.thumbnail
                          ? <img alt="" src={itemData.thumbnail} className="videoThumbnail" />
                          : <div className="videoThumbnail" />)}
                  <div className="display-item__details">
                      <h2>{itemData.title}</h2>
                      <div className="display-item__info">
                          <p>{`${readableViewsNumber}`}</p>
                      </div>
                      <div className="display-item__bottom-bar">
                          <div className="ex-icon" />
                      </div>
                  </div>
              </div>
          );
      }
      return (
          <div className="display-item error-item">
              <div>ITEM NOT AVAILABLE</div>
          </div>
      );
  }
}

displayItem.propTypes = {
    itemData: PropTypes.shape({
        title: PropTypes.string,
        thumbnail: PropTypes.string,
        itemId: PropTypes.string,
        views: PropTypes.number,
        source: PropTypes.string,
        type: PropTypes.string,
        url: PropTypes.string,
    }),
};

export default displayItem;

