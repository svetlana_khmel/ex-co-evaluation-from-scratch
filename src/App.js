import React, { Component, lazy, Suspense } from 'react';
import axios from 'axios';
import {
    BrowserRouter as Router,
} from 'react-router-dom';

import './App.css';

import Navbar from './components/Navbar/Navbar';

//const ItemsList = lazy(() => import('./components/ItemsList/ItemsList')); //doesn't work on AWS
//import ItemsList from './components/ItemsList/ItemsList';

if (process.env.NODE_ENV !== 'production') {
   console.log('Looks like we are in development mode!');
}

class App extends Component {
    constructor(props) {
        super(props);

        this.state = { items: [] };
    }

    componentDidMount() {
        this.getItems().then((res) => {
            this.setState({ items: res });
        });
    }

    async getItems() {
        const response = await axios.get('../../public/data/itemsfeed.json');
        console.log('..... ', response);
        return response.data.items;
    }

    render() {
        const { items } = this.state;
        return (
            <div className="app">
                <Navbar />
                <main>
                    <Router>
                        {/*<Suspense fallback="Loading..">*/}
                            {/*<ItemsList items={items} />*/}
                        {/*</Suspense>*/}
                    </Router>
                </main>
            </div>
        );
    }
}

export default App;
