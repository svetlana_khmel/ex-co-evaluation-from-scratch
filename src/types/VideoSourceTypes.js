import React from 'react';

export const VideoSourceTypes = [
    {
        name: 'playbuzz',
        id: 'videoId',
        getPlayer: (id) => {
            const video = (
                <div
                    className="playbuzz"
                    data-id={id}
                    data-show-share="false"
                    data-show-info="false"
                    data-comments="false"
                />
            );
            return video;
        },
        getIcon: () => <PlaybuzzLogo />,
    },
    {
        name: 'url',
        id: 'url',
        getPlayer: url => (
            <div className="url-source">
                <iframe
                    title="url"
                    height="210"
                    src={url}
                    frameBorder="0"
                />
            </div>
        ),
        getIcon: () => <UrlLogo />,
    },
    {
        name: 'facebook',
        id: 'videoId',
        getPlayer: (id) => {
            const src = `https://www.facebook.com/video/embed?video_id=${id}`;
            return (
                <div className="facebook-source">
                    <iframe
                        title="facebook"
                        height="210"
                        src={src}
                        frameBorder="0"
                    />
                </div>
            );
        },
        getIcon: () => <FacebookLogo />,
    },
    {
        name: 'youtube',
        id: 'videoId',
        getPlayer: (id) => {
            const src = `https://www.youtube.com/embed/${id}`;
            return (
                <div className="youtube-source">
                    <iframe
                        height="210"
                        title="youtube"
                        src={src}
                    />
                </div>
            );
        },
        getIcon: () => <YoutubeLogo />,
    },
];
